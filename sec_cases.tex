\section{Compatibility}

\begin{frame}{Compatible Satisfaction Systems}

    \begin{itemize}
        \item<1-> Can we always define eviction and reception?
        \item<2-> No! \(\FRsups\) or \(\FRsubs\) can be empty
        \item<3-> Example: Horn logic without \(\bot\)
        \item<4-> Corollary~\cite{Guimaraes2023}: if \(\FRsets(\logsys)\) is finite then:
            \begin{itemize}
                \item \(\logsys\) is eviction-compatible iff \(\emptyset \in \FRsets(\logsys)\)
                \item \(\logsys\) is reception-compatible iff \(\mUni \in \FRsets(\logsys)\)
            \end{itemize}
    \end{itemize}

\end{frame}

\begin{frame}{Propositional 3-Valued Logics}

    \begin{description}
        \item[Truth values:] \(\vfalse < \vunk < \vtrue\)
        \item[\(\llangprop\):] propositional formulae
        \item[\(\mUniKleene\):] $v: \llang \to \{\vfalse, \vunk, \vtrue\}$ s.t.\
            \begin{itemize}
                \item
                    \begin{align*}
                        v(\neg{\varphi}) = 
                        \begin{cases}
                            \vtrue, &\text{ if } v(\varphi) = \vfalse\\
                            \vunk, &\text{ if } v(\varphi) = \vunk\\
                            \vfalse, &\text{ if } v(\varphi) = \vtrue
                        \end{cases}
                    \end{align*}
                \item $v(\varphi \land \psi) = \min_<(\{v(\varphi), v(\psi)\})$
                \item $v(\varphi \lor \psi) = \max_<(\{v(\varphi), v(\psi)\})$
            \end{itemize}
    \end{description}

\end{frame}

\begin{frame}{Kleene's and Priest's 3-Valued Logics}

    \begin{columns}[t]
        \column{0.5\textwidth}
        {\Large{\textbf{Kleene's}}}

        \(\logsyskleene = (\llangprop,  \mUniKleene,  \models_{K3})\)

        \begin{align*}
            v &\models_{K3} \varphi \text{ iff } v(\varphi) = \vtrue
        \end{align*}

        \begin{itemize}
            \item \(\emptyset \in \FRsets(\logsyskleene)\)
            \item \(\mUniKleene \in \FRsets(\logsyskleene)\)
        \end{itemize}

        \column{0.5\textwidth}
        {\Large{\textbf{Priest's}}}

        \(\logsyspriest = (\llangprop,  \mUniKleene,  \models_{P3})\)

        \begin{align*}
            v &\models_{P3} \varphi \text{ iff } v(\varphi) \neq \vfalse
        \end{align*}

        \begin{itemize}
            \item {\alert{\(\emptyset \not\in \FRsets(\logsyskleene)\)}}
            \item \(\mUniKleene \in \FRsets(\logsyskleene)\)
        \end{itemize}
    \end{columns}

    \vspace{1cm}

    \begin{center}
        \(v_\vunk(\varphi) = \vunk, \forall \varphi\) \(v_\vunk\) is a model of every base in \(\logsyspriest\)
    \end{center}

\end{frame}

\begin{frame}{Propositional Gödel Logic}

\(\logsysfuzzy = (\llangfuzzy, \mUnifuzzy, \modelsfuzzy)\) with \(\theta \in {\left(0, 1\right]}\)
    \begin{itemize}
        \item[\(\llangfuzzy\):] propositional formulas over \(\propatoms\) with \(\land\), \(\lor\), \(\neg\), and \(\rightarrow\);
        \item[\(\mUnifuzzy\):]{% 
                \(v : \llang \to [0, 1]\) s.t.\
                \begin{align*}
                    v(\neg{\varphi}) &= \begin{cases}
                                            1 & \text{if } v(\varphi) = 0,\\
                                            0 & \text{otherwise;}
                                        \end{cases}\\
                    v(\varphi \land \psi) &= \min(v(\varphi), v(\psi));\\
                    v(\varphi \lor \psi) &= \max(v(\varphi), v(\psi));\\
                    v(\varphi \rightarrow \psi) &= \begin{cases}
                                                    1 & \text{if } v(\varphi) \leq v(\psi),\\
                                                    v(\psi) & \text{otherwise; and}
                                                \end{cases}
                \end{align*}
        }
    \item[\(\modelsfuzzy\):] \(v \modelsfuzzy \baseb\) iff \(\baseb = \emptyset\) or \(v(\bigwedge_{\varphi \in B} \varphi) \geq \theta\)
    \end{itemize}

\end{frame}

\begin{frame}{Compatibilities for Gödel Logic}
    \begin{itemize}
        \item<1-> \(\mUnifuzzy\) is infinite now! 
        \item<2-> However, \(v \modelsfuzzy \baseb\) depends only on the total preorder defined over finitely many symbols 
        \item<3-> \(\FRsets(\logsysfuzzy)\) is finite
        \item<4-> \(\modelsof{\{\neg{a} \land a\}} = \emptyset\) and \(\modelsof{\{\}} = \mUnifuzzy\)
        \item<5-> Therefore, \(\logsysfuzzy\) is eviction- and reception-compatible
    \end{itemize}
\end{frame}

\begin{frame}{Infinite \(\FRsets(\logsys)\)}
    \(\logsys_{q} = (\llang_q, \mUni_q, \models_q)\)
        \begin{itemize}[nosep]
            \item \(\llang_q = \{[x, y] \mid x, y \in \mathbb{Q} \text{ and } x \leq y\}\)
            \item \(\mUni_q = \mathbb{Q}\)
            \item \(z \models_q \baseb\) iff \(x \leq z \leq y\) for every \([x, y] \in \baseb\).
        \end{itemize}

        Example:

        \begin{align*}
            0.3 \models_q \left\{\left[0, 0.25\right], \left[0.12, 0.80\right]\right\}
        \end{align*}
\end{frame}

\begin{frame}{Too Many Candidates}

        We can have incompatibility even if \(\{\emptyset, \mUni\} \in \FRsets(\logsys)\)
        \begin{align*}
            \logsys_{q} = (\llang_q, \mUni_q, \models_q)
        \end{align*}

        \begin{align*}
            \mCon(\{[0, 1]\}, \{1\}) = [0, 0.9\only<2->{9}\only<3->{9}\only<4->{9\dots?}]
        \end{align*}

    \begin{visibleenv}<5->
        \begin{align*}
            \mExp(\{[0.5, 1]\}, \{q \in \mathbb{Q} \mid 0.2 < q < 0.5\}) = [?, 1]
        \end{align*}
    \end{visibleenv}

    \begin{visibleenv}<6->
        \resizebox{\textwidth}{!}{%
            \input{continuous.tex} 
        }
    \end{visibleenv}
\end{frame}


\begin{frame}{Description Logics}

    \begin{itemize}
        \item Fragments of guarded dyadic FOL (usually) 
        \item Good computational properties (usually decidable)
        \item Each fragment has a different expressivity and complexities
        \item Formalisation of OWL ontologies (used in data integration, Medical fields, Biology)
    \end{itemize}
\end{frame}

\begin{frame}{The Description Logic \(\ALC\): Signature and Constructors}
    \begin{description}[nosep]
        \item[Concept names (\(\NC\)):] \(Person\), \(Student\), \(University\)
        \item[Role names (\(\NR\)):] \(studiesAt\), \(supervisorOf\)
        \item[Individual names (\(\NI\)):] \(\mathtt{Ann}\), \(\mathtt{UiB}\)
    \end{description}

    \[
        C::=  \top \mid A \mid \neg C \mid (C \sqcap C) \mid \exists r.C
    \]

    Complex concepts
    \begin{itemize}
        \item \(Person \sqcap \neg{Student}\)
        \item \(\exists worksAt.\top\)
        \item \(Lawyer \sqcap \neg{\exists hasPet.Dog}\)
    \end{itemize}
\end{frame}

\begin{frame}{The Description Logic \(\ALC\): Formulae and Semantics}
    \begin{description}[nosep]
        \item[Concept Inclusions:] \(Student \sqsubseteq Person \sqcap \exists studies.\top\), \(Professor \sqsubseteq \exists worksAt.University\)
        \item[Concept Assertions:] \(Student(\mathtt{Ann})\), \(University(\mathtt{UiB})\)
        \item[Role Assertions:] \(worksAt(\mathtt{Bob}, \mathtt{UiB})\), \(studies(\mathtt{Carol}, \mathtt{Logic})\)
    \end{description}

    Models are interpretations over the signature:

    \begin{align*}
        \I = (\eqnmarkbox[mycolor1]{node1}{\Delta^{\I}}, \eqnmarkbox[mycolor2]{node2}{\cdot^{\I}})
    \end{align*}
    \annotate[]{below, left}{node1}{Domain}
    \annotate[]{below, right}{node2}{Mapping function}
\end{frame}


\begin{frame}[fragile]{What do we know?}
    % TODO: add the table and recap the corollary

\begin{table}[htb]
    \centering
    \begin{tabular}{@{}lll@{}}
        \toprule
        \multirow{2}{*}{Satisfaction System} & \multicolumn{2}{c}{Compatible} \\ \cmidrule(l){2-3}
                                             & \Mconnm{}      & \Mexnm{}      \\ \midrule
        \(\logsysprop\)                    & \textcolor{oibluishgreen}{Yes}            & \textcolor{oibluishgreen}{Yes}           \\
        \(\logsyshorn\)                    & \textcolor{oibluishgreen}{Yes}            & \textcolor{oibluishgreen}{Yes}           \\
        \(\logsyskleene\)                      & \textcolor{oibluishgreen}{Yes}            & \textcolor{oibluishgreen}{Yes}           \\
        \(\logsyspriest\)                      & \textcolor{oivermillion}{No}            & \textcolor{oibluishgreen}{Yes}           \\
        \(\logsysfuzzy\)                     & \textcolor{oibluishgreen}{Yes}            & \textcolor{oibluishgreen}{Yes}           \\
        \(\logsysltlx\)                       & \textcolor{oivermillion}{No}             & \textcolor{oibluishgreen}{Yes}           \\
        \(\logsysDLABox\)                  & \textcolor{oibluishgreen}{Yes}             & \textcolor{oivermillion}{No}            \\
        \(\logsysDLLITE\)\footnote{with finite signature}                  & \textcolor{oibluishgreen}{Yes}             & \textcolor{oibluishgreen}{Yes}            \\
        \(\logsysALC\)                  & \textcolor{oivermillion}{No}             & \textcolor{oivermillion}{No}            \\
        \bottomrule
    \end{tabular}
\end{table}
\end{frame}


\begin{frame}{General Criteria}

    \begin{columns}[t]
        \column{0.5\textwidth}
        {\Large Eviction}
        \begin{itemize}
            \item \(\mSet \in \FRsets(\logsys)\)
            \item \(\mSet\) has a FR immediate predecessor
            \item There is no superset of \(\mSet\) in \(\FRsets(\logsys)\)
        \end{itemize}

        \column{0.5\textwidth}
        {\Large Reception}
        \begin{itemize}
            \item \(\mSet \in \FRsets(\logsys)\)
            \item \(\mSet\) has a FR immediate successor
            \item There is no subset of \(\mSet\) in \(\FRsets(\logsys)\)
        \end{itemize}
    \end{columns}
\end{frame}

