\section{Finite Based Change via Models}

\begin{frame}{Finite Based Contraction and Expansion via Models~\cite{Guimaraes2023}}

    \begin{itemize}
        \item To appear in AAAI 2023
        \item Joint work with Ana Ozaki and Jandson S. Ribeiro
    \end{itemize}
\end{frame}

\begin{frame}{Logic as a Satisfaction System}
    \begin{equation*}
        \eqnmark[mycolor1]{node1}{\logsys} = (
            \eqnmarkbox[mycolor2]{node2}{\llang}, 
            \eqnmarkbox[mycolor3]{node3}{\mUni},
            \eqnmarkbox[mycolor4]{node4}{\models}
        )
    \end{equation*}
    \annotate[]{below, left}{node1}{Satisfaction System}
    \annotate[yshift=1em]{above, left}{node2}{Language}
    \annotate[yshift=1em]{above, right}{node3}{Universe of Models}
    \annotate[]{below, right}{node4}{Satisfaction Relation}

    \begin{visibleenv}<2->
        Example: propositional logic with signature \(\{p, q\}\)

        \begin{description}
            \item[\(\llangprop\):] \(p\), \(q\), \(\neg{p}\), \(p \land q\), \(p \land p\), \dots
            \item[\(\mUniprop\):] \(\{\hmoda, \hmodb, \hmodc, \hmodd\}\)
            \item[\(\models_\text{Prop}\):] the usual satisfaction relation
        \end{description}
    \end{visibleenv}
\end{frame}



\begin{frame}{Finitely Representable Sets of Models}

    \(X \in \FRsets(\logsys)\) iff
    \begin{itemize}[nosep]
        \item \(X \subseteq \mUni\)
        \item There is \(\baseb \in \finitepwset(\llang)\) with \(\modelsof{\baseb} = X\)
    \end{itemize}

    \begin{visibleenv}<2->
        \begin{center}
            \input{fig_linkedcubes_prop_noc.tex}
        \end{center}
    \end{visibleenv}

\end{frame}

\begin{frame}{Selecting Sets of Models}

    FR selection function: \(\FRsel : \powerset^{*}(\FRsets(\logsys)) \to \FRsets(\logsys)\)

    \begin{align*}
        f(\{\{\hmoda\}, \{\hmodb, \hmodc\}, \mUni\}) = \{\hmodb, \hmodc\}
    \end{align*}

    \begin{center}
        \input{fig_linkedcubes_horn_noc.tex}
    \end{center}

\end{frame}

\begin{frame}{\(\FRsubs\)}

    Maximal, finitely representable subsets

    \begin{align*}
        \FRsubs(\{\hmoda, \hmodb, \hmodc\}, \logsyshorn) &= \{\eqnmarkbox[mycolor1]{node1}{\{\hmoda, \hmodb, \hmodc\}}\}\\
        \onslide<2->{\FRsubs(\{\hmodb, \hmodc, \hmodd\}, \logsyshorn) &= \{\eqnmarkbox[mycolor2]{node2}{\{\hmodb, \hmodd\}}, \eqnmarkbox[mycolor4]{node3}{\{\hmodc, \hmodd\}}\}}
    \end{align*}

    \annotate[yshift=1em]{above, right}{node1}{\(\modelsof{p \land q \rightarrow \bot}\)}
    \begin{visibleenv}<2->
        \annotate[]{below, left}{node2}{\(\modelsof{\bot \rightarrow q}\)}
        \annotate[]{below, right}{node3}{\(\modelsof{\bot \rightarrow p}\)}
    \end{visibleenv}

    \begin{center}
        \input{fig_linkedcubes_horn_subs.tex}
    \end{center}

\end{frame}

\begin{frame}{Eviction}

    \begin{description}[nosep]
        \item[\(\FRsel\):] selects a finitely representable set of models
        \item[\(\baseb\):] current base
        \item[\(\mSet\):] models to remove
    \end{description}

  \begin{align*}
      \modelsof{ \mCon_{\FRsel}(\baseb, \mSet)} = \FRsel(\FRsubs(\modelsof{\baseb}
         \setminus \mSet, \logsys)).
    \end{align*}

\end{frame}

\begin{frame}{Eviction: Example with \(\logsyshorn\)}

    \begin{align*}
        \modelsof{\mCon(\eqnmarkbox[mycolor2]{node1}{\{p \land q \rightarrow \bot\}}, \{\hmoda\})} &= \FRsel(\FRsubs(\eqnmarkbox[mycolor5]{node2}{\{\hmodb, \hmodc\}})) = \\
        \FRsel(\{\eqnmarkbox[mycolor6]{node1}{\{\hmodb\}}, \{\hmodc\}\}) &= \modelsof{\{\neg{p}, q \}}
    \end{align*}

    \begin{center}
        \input{fig_linkedcubes_horn_subs2.tex}
    \end{center}

\end{frame}

\begin{frame}{Eviction: Postulates}

    \begin{align*}
        \modelsof{ \mCon_{\FRsel}(\baseb, \mSet)} = \FRsel(\FRsubs(\modelsof{\baseb}
        \setminus \mSet, \logsys)).
    \end{align*}

    \begin{description}[leftmargin=*]
        \item[(success)] all the required models are removed%\(\mSet \cap \modelsof{\mCon(\baseb, \mSet)} = \emptyset\).
        \item[(inclusion)]<2-> no new models are added %\(\modelsof{\mCon(\baseb, \mSet)} \subseteq \modelsof{\baseb}\).
        \item[(finite retainment)]<3-> only lose extra models if there is no better FR solution (w.r.t.\ \(\subseteq\))
        \item[(uniformity)]<4-> if \(\FRsubs(\modelsof{\baseb} \setminus \mSet, \logsys) = \FRsubs(\modelsof{\baseb'} \setminus \mSet', \logsys)\), then the final result is the same
    \end{description}
\end{frame}

\begin{frame}{\(\FRsups\)}
    Minimal, finitely representable supersets

    \begin{align*}
        \FRsups(\{\hmoda, \hmodb, \hmodc\}, \logsyshorn) &= \{\eqnmarkbox[mycolor1]{node1}{\{\hmoda, \hmodb, \hmodc\}}\}\\
        \FRsups(\{\hmodb, \hmodc\}, \logsyshorn) &= \{\{\eqnmarkbox[mycolor1]{node2}{\{\hmoda, \hmodb, \hmodc\}}\}
    \end{align*}

    \annotate[yshift=1em]{above}{node1}{\(\modelsof{p \land q \rightarrow \bot}\)}
    %\begin{visibleenv}<2->
        %\annotate[]{below, left}{node2}{\(\modelsof{\bot \rightarrow q}\)}
        %\annotate[]{below, right}{node3}{\(\modelsof{\bot \rightarrow p}\)}
    %\end{visibleenv}

    \begin{center}
        \input{fig_linkedcubes_horn_sups.tex}
    \end{center}

\end{frame}

\begin{frame}{Reception}

    \begin{description}[nosep]
        \item[\(\FRsel\):] selects a finitely representable set of models
        \item[\(\baseb\):] current base
        \item[\(\mSet\):] models to add
    \end{description}

  \begin{align*}
      \modelsof{ \mExp_{\FRsel}(\baseb, \mSet)} = \FRsel(\FRsups(\modelsof{\baseb}
         \cup \mSet, \logsys)).
    \end{align*}

\end{frame}

\begin{frame}{Reception: Example with \(\logsyshorn\)}

    \begin{align*}
        \modelsof{\mExp(\eqnmarkbox[mycolor2]{node1}{\{p, \neg{q}\}}, \{\hmodb, \hmodd\})} &= \FRsel(\FRsubs(\eqnmarkbox[mycolor5]{node2}{\{\hmodb, \hmodc, \hmodd\}})) = \\
        \FRsel(\{\eqnmarkbox[mycolor6]{node1}{\mUni}\}) &= \modelsof{\{\}}
    \end{align*}

    \begin{center}
        \input{fig_linkedcubes_horn_sups2.tex}
    \end{center}

\end{frame}

\begin{frame}{Reception: Postulates}

    \begin{align*}
        \modelsof{ \mExp_{\FRsel}(\baseb, \mSet)} = \FRsel(\FRsups(\modelsof{\baseb}
        \cup \mSet, \logsys)).
    \end{align*}

    \begin{description}[leftmargin=*]
        \item[(success)] all the required models are added%\(\mSet \cap \modelsof{\mCon(\baseb, \mSet)} = \emptyset\).
        \item[(persistence)]<2-> no models are removed %\(\modelsof{\mCon(\baseb, \mSet)} \subseteq \modelsof{\baseb}\).
        \item[(finite temperance)]<3-> only add extra models if there is no better FR solution (w.r.t.\ \(\subseteq\))
        \item[(uniformity)]<4-> if \(\FRsubs(\modelsof{\baseb} \cup \mSet, \logsys) = \FRsubs(\modelsof{\baseb'} \cup \mSet', \logsys)\), then the final result is the same
    \end{description}
\end{frame}

\begin{frame}{Eviction \& Reception: Propositional Case}

    \begin{align*}
        \evcprop(\baseb,\mSet) &= \bigvee_{v \in \modelsof{\baseb} \setminus \mSet} \left(\bigwedge_{v(a) = \vtrue} a \wedge \bigwedge_{v(a) = \vfalse} \neg{a} \right)\\
        \rcpprop(\baseb,\mSet) &= \bigvee_{v \in \modelsof{\baseb} \cup \mSet} \left(\bigwedge_{v(a) = \vtrue} a \wedge \bigwedge_{v(a) = \vfalse} \neg{a} \right)
    \end{align*}

\end{frame}

\begin{frame}[standout]
    Is it always this easy?
\end{frame}
